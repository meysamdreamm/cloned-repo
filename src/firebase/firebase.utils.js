import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyAiOqsaOI67FTPwH_5FZNvTqP7yNc-OIyU",
  authDomain: "crwn-db-8bd3d.firebaseapp.com",
  databaseURL: "https://crwn-db-8bd3d.firebaseio.com",
  projectId: "crwn-db-8bd3d",
  storageBucket: "crwn-db-8bd3d.appspot.com",
  messagingSenderId: "980974530974",
  appId: "1:980974530974:web:03ab004be4b7f79446668f",
  measurementId: "G-020LB998KE",
};

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);
  
  const snapShot = await userRef.get();
  
  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData,
      });
    } catch (error) {
      console.log("error create user", error.message);
    }
  }
  return userRef;
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
